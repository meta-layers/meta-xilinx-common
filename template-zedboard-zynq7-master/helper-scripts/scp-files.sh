#!/bin/bash

DEPLOY="zedboard-zynq7-master/tmp/deploy/images/zedboard-zynq7"

pushd /workdir/build/${DEPLOY}

SOURCE_1="*"

TARGET_1="student@192.168.42.60:/home/student/projects/${DEPLOY}"

set -x
#scp -r ${SOURCE_1} ${TARGET_1}
rsync -avz -e ssh ${SOURCE_1} ${TARGET_1}
set +x

popd
